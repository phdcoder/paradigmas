if (!crossOriginIsolated) {
    throw new Error('Cannot use SharedArrayBuffer');
}
const worker = new Worker('worker15.js');


const buffer = new SharedArrayBuffer(1024);
const buffer2 = new SharedArrayBuffer(512);
const view = new Int32Array(buffer);
const view2 = new Uint8Array(buffer2);

const enc= new TextEncoder();
let encoded=enc.encode("hello from main worker!");
encoded.forEach((element,i)=> view2[i]= element );
worker.postMessage([buffer,buffer2]);