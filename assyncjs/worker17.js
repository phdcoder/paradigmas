self.onmessage = ({ data }) => {
    const { buffer, start, end, readOffset, writeOffset, workerIndex } = data;
    const view = new Int32Array(buffer);
    for (let j = start; j < end; j += 2) {
        view[writeOffset + j/2] = Math.min(view[readOffset + j], view[readOffset + j + 1]);
    }
    self.postMessage(workerIndex);
}