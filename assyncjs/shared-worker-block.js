// shared-worker.js
let tabIsOpen = 0;

self.onconnect = e =>  {
    const port = e.ports[0];
    tabIsOpen ++;
    port.onmessage = e => {
        if (e.data === 'check') {
            port.postMessage(tabIsOpen);
        }
        if (e.data === 'closing') {
            console.log ("fechando uma aba");
            tabIsOpen --;
        }
    };
};








