let msgUint8;
let hashBuffer;
let hashArray;
let hashHex;

self.onmessage = ({data: buffer}) => {
    const lock = new Int32Array(buffer);
    str="ababd";
    console.log("vou executar")
    digestMessage(str).then( hashresult =>{
        console.log("executando hash")
        if("hash"===hashresult){
            postMessage(str);
        }
        lock[0]=0;
    });
}

async function digestMessage(message) {
    msgUint8 = new TextEncoder().encode(message);                           // encode as (utf-8) Uint8Array
    hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);           // hash the message
    hashArray = Array.from(new Uint8Array(hashBuffer));                     // convert buffer to byte array
    hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join(''); // convert bytes to hex string
    return hashHex
}

