window.sqlite3InitModule().then(function(sqlite3){
    // The module is now loaded and the sqlite3 namespace
    // object was passed to this function.
    console.log("sqlite3:", sqlite3);
});

const db = new sqlite3.Database(':memory:'); // Use ":memory:" para um banco de dados em memória

// Crie a tabela e insira alguns dados fictícios
db.serialize(() => {
db.run('CREATE TABLE nome (id INT, nome TEXT)');
const stmt = db.prepare('INSERT INTO nome VALUES (?, ?)');
stmt.run(1, 'Alice');
stmt.run(2, 'Bob');
stmt.run(3, 'Charlie');
stmt.finalize();

// Função para consultar os nomes na tabela
function consultarNomes() {
return new Promise((resolve, reject) => {
db.all('SELECT nome FROM nome', (err, rows) => {
if (err) {
reject(err);
} else {
resolve(rows);
}
});
});
}

// Chame a função de consulta usando Promises
consultarNomes()
.then((result) => {
console.log('Nomes no banco de dados:');
result.forEach((row) => {
console.log(row.nome);
});
})
.catch((err) => {
console.error('Erro ao consultar nomes:', err);
})
.finally(() => {
db.close();
});
});
