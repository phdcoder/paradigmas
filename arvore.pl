progenitor(maria,jose).
progenitor(joao,jose).
progenitor(joao,ana).
progenitor(jose,julia).
progenitor(jose,iris).
progenitor(iris,jorge).
progenitor(ana,leticia).
masculino(joao).
masculino(jose).
masculino(jorge).
feminino(maria).
feminino(julia).
feminino(ana).
feminino(iris).
feminino(leticia).
filho(X,Y):-masculino(X), progenitor(Y,X).
avo(X,Y):-masculino(X), progenitor(X,W),progenitor(W,Y).
irma(X,Y):-feminino(X),progenitor(Z,X), progenitor(Z,Y), X\= Y.
prima(X,Y):-feminino(X), progenitor(W,Z),progenitor(Z,X), progenitor(W,K),
    progenitor(K,Y), K\=Z.
