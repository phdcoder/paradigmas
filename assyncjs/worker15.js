self.onmessage = ( {data: buffer}) => {
    const view = new Int32Array(buffer[0]);
    const view2 = new Uint8Array(buffer[1]);

    const dec = new TextDecoder();
    let back= view2.slice(0);
    let string= dec.decode(back);
    console.log('buffer 2 in worker: ', string);
};
