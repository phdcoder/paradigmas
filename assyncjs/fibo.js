self.onmessage = e => {
    let num = Number(e.data);
    fibonacci(num);
};

function fibonacci(num) {
    let x,b,a,temp,copy;
    console.log("Iniciando o processo de cálculo");
    for (x=1;x<=10000000;x++) {
        a = 1; b = 0;
        copy=num;
        while (copy >= 0) {
            temp = a;
            a = a + b;
            b = temp;
            copy--;
        }
    }
   self.postMessage(a);
}
