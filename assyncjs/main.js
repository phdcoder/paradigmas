const worker =  new Worker('fibo.js');
console.log("hello from main");
worker.postMessage('48');
worker.onmessage= msg =>{
    console.log("Valor do fibonacci: ",msg.data);
};

/*
const worker1 =  new Worker('fibo.js');
const worker2 =  new Worker('fibo.js');
const worker3 =  new Worker('fibo.js');
const worker4 =  new Worker('fibo.js');
console.log("hello from main");
worker1.postMessage('48');
worker2.postMessage('48');
worker3.postMessage('48');
worker4.postMessage('48');
worker1.onmessage= msg =>{
    console.log("Valor 1 do fibonacci: ",msg.data);
};
worker2.onmessage= msg =>{
    console.log("Valor 2 do fibonacci: ",msg.data);
};
worker3.onmessage= msg =>{
    console.log("Valor 3 do fibonacci: ",msg.data);
};
worker4.onmessage= msg =>{
    console.log("Valor 4 do fibonacci: ",msg.data);
};

*/