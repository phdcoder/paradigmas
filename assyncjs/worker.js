const alfabeto = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
    'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
let hash;
self.onmessage = (msg) => {
hash = msg.data[0];
crack( msg.data[1],0,0,0,0);
}

function crack (h,i,j,k,l){
    digestMessage( h+ alfabeto[i]+ alfabeto[j]+ alfabeto[k]+ alfabeto[l]).
    then( arr => {
        if (arr[0]===hash){
            postMessage(arr[1]);
        }else{
            l++;
            if ( l===26){
                l=0;
                k++;
            }
            if ( k===26){
                k=0;
                j++;
            }
            if ( j===26){
                j=0;
                i++;
            }
            if ( i===26){
                console.log(`terminado com  a letra ${h}`);
                return;
            }
            crack(h,i,j,k,l);
        }
    } )
}

async function digestMessage(senha) {
    const msgUint8 = new TextEncoder().encode(senha); // encode as (utf-8) Uint8Array
    const hashBuffer = await crypto.subtle.digest("SHA-256", msgUint8); // hash the message
    const hashArray = Array.from(new Uint8Array(hashBuffer)); // convert buffer to byte array
    const hashHex = hashArray
        .map((b) => b.toString(16).padStart(2, "0"))
        .join(""); // convert bytes to hex string
    return [hashHex,senha];
}



