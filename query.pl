:-dynamic capital/2.
capitais:- loadFile('C:\\e\\phd_coder\\Documents\\Facul\\paradigmas\\paradigmas2024\\base.pl'),
    format('Banco de capitais ~n'),
    repeat,
    perguntar(E),
    responder(E),
    parar(R),
    R=s,
    !,
    saveFile(capital,'C:\\e\\phd_coder\\Documents\\Facul\\paradigmas\\paradigmas2024\\base.pl').
loadFile(F):- exists_file(F),consult(F),true.
saveFile(P,F):-tell(F),listing(P),told.
perguntar(E):-format('Qual o estado que deseja saber a capital?'),read(E).
responder(E):- capital(E,C),!,format('A capital de ~w � ~w ~n',[E,C]).
responder(E):-format('nao sei ! qual � ?'),read(C),asserta(capital(E,C)).
parar(R):-format('Deseja Parar ? (s/n)'),read(R).






