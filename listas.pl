pertence(Elemento,[Elemento|_]).
pertence(Elemento,[_|Cauda]):- pertence(Elemento,Cauda),!.
ultimo(X,[X|[]]):-!.
ultimo(X,[_|C]):- ultimo(X,C).
concatena([X|[]],Y,[X|Y]):-!.
concatena([X|Z],Y, [X|W]):- concatena(Z,Y,W).
remov(_,[],[]).
remov(X,[X|Y],W):- remov(X,Y,W),!.
remov(X,[Z|Y],[Z|W]):- X\=Z,remov(X,Y,W),!.
